# selenium-cucumber-java #

BDD Test automation framework using Java, Cucumber and Selenium.

### Tool and Programming language Used ###

* Java as a programming language
* Cucumber as Behavior driven testing
* Rest assured for API testing
* Junit as test framework
* Maven as build tool
* slf4j for logging

### Features ###

* Framework is build using Cucumber as a BDD tool
* Used for running both web and API testcases
* Used Page factory for keeping page object
* Used Constants Java file
* Avoid hard coding values
* Used property file
* Framework can we easily extended with less modification required
* Used logger for printing necessary logs
* Using inbuilt cucumber Html report
* Capturing screenshots

### Environment Setup ###

* Java installed in the system
* Maven installed in the system

### Steps for executing feature file ###

* Import project in eclipse IDE
* Run mvn clean install for downloading dependency
* Run TestRunner class for executing feature file
* Screenshot are captured in target folder
* Html report in target/Cucumber-report folder