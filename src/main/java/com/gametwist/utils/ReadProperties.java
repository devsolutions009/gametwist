package com.gametwist.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.gametwist.utils.ReadProperties;

/**
 * This class is responsible for reading properties file. 
 *
 */
public class ReadProperties {
	
	private static Properties prop;
	
	static {
		try {
			prop = readPropertiesFile("src//test//resources//properties//qa.properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
     }
	
	public static String getProperty(String propertyName) {
		return prop.getProperty(propertyName);
	}

   public static Properties readPropertiesFile(String fileName) throws IOException {
      FileInputStream fis = null;
      Properties prop = null;
      try {
         fis = new FileInputStream(fileName);
         prop = new Properties();
         prop.load(fis);
      } catch(IOException e) {
    	  System.out.println("Exception occurred while reading properties file.." + e.getMessage());
         e.printStackTrace();
      }finally {
         fis.close();
      }
      
      return prop;
   }
}
