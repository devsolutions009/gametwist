package com.gametwist.utils.http;

public enum RequestType {
	POST, PUT, GET, PATCH, DELETE
}
