package com.gametwist.utils;

import java.io.File;

public class Constants {


    private Constants() {
            // restrict instantiation
    }

	public static final String CHROME = "chrome";
	public static final String FIREFOX = "firefox";
	public static final String BROWSER = "browser";
	public static final String BANK_NAME = "Bank Austria";
	public static final String GAME_TWIST_HOME_PAGE = "GameTwistHomePage";
	public static final String USER_NAME = "test_user";
	public static final String PIN = "123456";
	public static final String ERROR_MESSAGE = "Es sind Fehler aufgetreten!";
    public static final String APP_URL = "https://www.gametwist.com/en/";
    public static final String BASE_URL = "baseUrl";
    public static final String PAGE_TITLE = "Sample Store";
    public static final String PAYMENT_SUCCESS_MESSAGE = "Thank you for your purchase.";
    public static final String SUCCESS_MESSAGE = "Thank you for your purchase.";
    public static final String YY_MM_DD_HH_MM_SSS = "yyyy-MM-dd-hh_mm_ss";
    public static final String TRANSACTION_AMOUNT = "Rp 20,000";
    public static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_FIREFOX_DRIVER = "webdriver.gecko.driver";
    public static final String HOST_NAME = "Host name";
    public static final String LOCAL_HOST = "localhost";
    public static final String ENVIRONMENT = "Environemnt";
	public static final String QA = "QA";
	public static final String USER = "user";
	public static final String AUTOMATION_TEST_USER = "Automation Test User";
	
	// webdriver's path
	public static final String CHROMEDRIVER_PATH = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "driver" + File.separator + "chromedriver.exe";
	public static final String GECKODRIVER_PATH = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "driver" + File.separator + "geckodriver.exe";


}
