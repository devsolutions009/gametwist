package com.gametwist.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtils.class);

	/** It will wait for the provided seconds
	 * @param second
	 */
	public static void waitForSeconds(int second) {
		LOGGER.info("Waiting for {} seconds ", second);
		try {
			Thread.sleep(second * 1000);
		} catch (InterruptedException e) {
			LOGGER.error("!! Error occuured during sleep..");
			e.printStackTrace();
		}
	}
}
