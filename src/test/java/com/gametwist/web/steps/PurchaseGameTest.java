package com.gametwist.web.steps;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gametwist.service.core.TestContext;
import com.gametwist.service.model.LoginDetails;
import com.gametwist.service.model.Registration;
import com.gametwist.utils.ReadProperties;
import com.gametwist.utils.http.RequestType;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class PurchaseGameTest extends BaseStep {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseGameTest.class);
	private static final String BASE_URL = ReadProperties.getProperty("baseUrl");

	public PurchaseGameTest(TestContext testContext) {
		super(testContext);
	}

	@Given("^I login using valid credential$")
	public void validateLogin() {
		// arrange
		LoginDetails loginDetails = new LoginDetails("Testing7652", "Welcome_2_gametwist", true);
		// act
		Response response = getRestClient().submitRequest(BASE_URL + "/login-v1", RequestType.POST, loginDetails);
		String jwtTokenHeader = response.getHeader("x-nrgs-auth-token-jwt");
		LOGGER.info("JWT Token is: {}", jwtTokenHeader);
		getRestClient().request.header("Authorization", "Bearer " + jwtTokenHeader);
		getScenarioContext().setContext("jwtToken", jwtTokenHeader);
		
		// assert
		String statusLine = response.getStatusLine();
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}

	@When("I accept the consent")
	public void validateConsentType() {
		// arrange
		String endpoint = BASE_URL + "/consent/consent-v1?consentType=GeneralTermsAndConditions&accepted=true";
		// act
		Response response = getRestClient().submitRequest(endpoint, RequestType.POST);
		String statusLine = response.getStatusLine();
		// assert
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}

	@When("I valdiate the consent")
	public void getConsentType() {
		// arrange
		String endpoint = BASE_URL + "/consent/consent-v1?consentType=GeneralTermsAndConditions";
		// act
		Response response = getRestClient().submitRequest(endpoint, RequestType.GET);
		String statusLine = response.getStatusLine();
		// assert
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}

	@When("I validate registration")
	public void validateFullRegistration() {
		// arrange
		Registration registrationDetails = new Registration("Johnathaa", "Doeya", true, "AT", "Vienna", "1050", "Wiedner Hauptstraße 94", "43", "12345678", "squestion_name_of_first_pet", "Heena");
		// act
		Response response = getRestClient().submitRequest(BASE_URL + "/player/upgradeToFullRegistrationGT-v1", RequestType.POST, registrationDetails);
		LOGGER.info(response.prettyPrint());
		// assert
		if (response.getStatusCode() == 200) {
			LOGGER.info("User is sucessfully Registered");
		} else if (response.getStatusCode() == 403) {
			String messge = response.jsonPath().getString("message");
			Assert.assertEquals(messge, "Player already upgraded to full registration");
			String code = response.jsonPath().getString("code");
			Assert.assertEquals(code, "player-already-fully-registered");
		}

	}

	public @Then("I purchase the game") void validatePurchase() {
		String BASE_URL_PURCHASE = "https://payments-api-v1-at.greentube.com/gametwist.widgets.web.site/en/api";
		// arrange
		Map<String, String> requestParams = new LinkedHashMap<>();
		requestParams.put("item", "m");
		requestParams.put("paymentTypeId", "adyenEPS");
		requestParams.put("country", "AT");
		requestParams.put("landingUrl", "https://www.gametwist.com/en/?modal=shop");
		// act
		Response response = getRestClient().submitRequest(BASE_URL_PURCHASE+  "/purchase-v1", RequestType.POST, requestParams);
		String paymentRedirectUrl = response.jsonPath().getString("paymentRedirectUrl");
		getScenarioContext().setContext("paymentUrl", paymentRedirectUrl);
		LOGGER.info("Payment redirection URL is {} ",  paymentRedirectUrl);
		String statusLine = response.getStatusLine();
		// assert
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");

	}
}
