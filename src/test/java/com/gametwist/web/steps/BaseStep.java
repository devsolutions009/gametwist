package com.gametwist.web.steps;

import org.openqa.selenium.WebDriver;

import com.gametwist.service.core.DriverManager;
import com.gametwist.service.core.ScenarioContext;
import com.gametwist.service.core.TestContext;
import com.gametwist.utils.http.RestClient;

/**
 * This class can be extended to any step definition file to get the context
 */
public class BaseStep {

	private ScenarioContext scenarioContext;
	private DriverManager driverManager;
	private RestClient restClient;

	public BaseStep(TestContext testContext) {
		scenarioContext = testContext.getScenarioContext();
		driverManager = testContext.getdriverManger();
		restClient = new RestClient();
	}

	public ScenarioContext getScenarioContext() {
		return scenarioContext;
	}

	public WebDriver getDriver() {
		return driverManager.getDriver();
	}

	public RestClient getRestClient() {
		return restClient;
	}

	public void captureScreenshot(WebDriver driver, String screenshotName) {
        driverManager.captureScreenshot(driver, screenshotName);
    }
}
