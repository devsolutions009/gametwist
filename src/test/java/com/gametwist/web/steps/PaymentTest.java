package com.gametwist.web.steps;

import static com.gametwist.utils.Constants.APP_URL;
import static com.gametwist.utils.Constants.BANK_NAME;
import static com.gametwist.utils.Constants.ERROR_MESSAGE;
import static com.gametwist.utils.Constants.PIN;
import static com.gametwist.utils.Constants.USER_NAME;
import static com.gametwist.utils.Constants.GAME_TWIST_HOME_PAGE;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gametwist.service.core.TestContext;
import com.gametwist.utils.CommonUtils;
import com.gametwist.web.pages.PaymentPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PaymentTest extends BaseStep {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentTest.class);
	PaymentPage paymentPage;

	public PaymentTest(TestContext testContext) {
		super(testContext);
	}

	@When("^user is on Payment Home Page$")
	public void ValidateNavigationToHomePageFromPayment() {
		String paymentUrl = (String) getScenarioContext().getContext("paymentUrl");
		LOGGER.info("Payment url is : {}", paymentUrl);
		getDriver().get(paymentUrl);
	}

	@Then("^User validates invalid payment details and on cancel navigate back to Game Twist home page$")
	public void userEntersPaymentDetails() {
		paymentPage = new PaymentPage(getDriver());
		paymentPage.clickOnNextButton();
		paymentPage.selectBankFromDropdown(BANK_NAME);
		paymentPage.clickOnSubmitButton();
		paymentPage.enterLoginDetails(USER_NAME, PIN);
		String actualErrorMsg = paymentPage.errorMessage();
		Assert.assertEquals(actualErrorMsg, ERROR_MESSAGE);
		paymentPage.clickCancelButton();
		CommonUtils.waitForSeconds(12);
		paymentPage.navigateHome(APP_URL);
		CommonUtils.waitForSeconds(10);
		String actualUrl = paymentPage.getCurrentUrl();
		Assert.assertTrue(actualUrl.contains(APP_URL));
		captureScreenshot(getDriver(), GAME_TWIST_HOME_PAGE);
	}

}
