package com.gametwist.web.steps;

import com.gametwist.service.core.TestContext;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	TestContext testContext;

	public Hooks(TestContext context) {
		testContext = context;
	}

	@Before
	public void BeforeSteps() {
		// anything before the test
	}

	@After
	public void AfterSteps() {
		testContext.getdriverManger().closeDriver();
	}
}
