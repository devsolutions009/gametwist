package com.gametwist.web.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(glue = { "com.gametwist.web.steps" }, features = {
		"src/test/resources/FeatureFiles/Payment_E2ETest.feature" },
plugin = { "pretty", "json:target/cucumber-reports/Cucumber.json",
		"junit:target/cucumber-reports/Cucumber.xml",
		"html:target/cucumber-reports"},
		monochrome = true)

public class TestRunner {

}
