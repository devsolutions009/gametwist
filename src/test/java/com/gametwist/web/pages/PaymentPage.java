package com.gametwist.web.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This class contains all Payment page locators and actions.
 *
 */

public class PaymentPage {

	WebDriver driver; 
  
    public PaymentPage(WebDriver driver) { 
    	PageFactory.initElements(driver, this);
        this.driver = driver; 
    }
    
    @FindBy(how = How.XPATH, using = "//button//span[text()='Next']")
    WebElement nextButton;
    
    @FindBy(how = How.XPATH, using = "//select[@id='epsIssuerSelect']")
    WebElement bankDropdown;
    
    @FindBy(how = How.XPATH, using = "//input[@id='mainSubmit']")
    WebElement submitButton;
    
    @FindBy(how = How.XPATH, using = "//input[@name='gebForm:verf_ID']")
    WebElement username;
    
    @FindBy(how = How.XPATH, using = "//input[@name='gebForm:pin_ID']")
    WebElement password;
    
    @FindBy(how = How.XPATH, using = "//input[@name='gebForm:LoginCommandButton']")
    WebElement loginButton;
    
    @FindBy(how = How.XPATH, using = "//div[contains(@class,'messagebox_error')]//div[contains(@class,'messagebox_topic')]//span")
    WebElement errorMessage;
    
    @FindBy(how = How.XPATH, using = "//ul//li//a[@id='gebForm:j_id71']")
    WebElement cancelButtonPaymentPage;
    
    @FindBy(how = How.XPATH, using = "//div//a[@id='gebForm:j_id74']")
    WebElement cancelButton;
    
    public void clickOnNextButton() { 
    	new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(nextButton)).click();
    }
    
    public void selectBankFromDropdown(String bankName) { 
    	Select select = new Select(bankDropdown);
		select.selectByVisibleText("Bank Austria");
    }
    
    public void clickOnSubmitButton() { 
    	submitButton.click(); 
    }
    
    public void enterLoginDetails(String userName, String pin) { 
    	username.sendKeys(userName);
    	password.sendKeys(pin);
    	new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(loginButton)).click();
    }
    
    public String errorMessage() { 
    	return new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(errorMessage)).getText();
    }
    
    public void clickCancelButton() { 
    	cancelButtonPaymentPage.click();
    }
    
    public void navigateHome(String expectedUrl) {
    	String getCurrentUrl = driver.getCurrentUrl();
    	if(!getCurrentUrl.contains(expectedUrl)) {
    		Actions action = new Actions(driver);
    		action.sendKeys(Keys.PAGE_DOWN).build().perform();
    		new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOf(cancelButton)).click();
    	}
    }
    
    public String getCurrentUrl() {
    	return driver.getCurrentUrl();
	}
}
