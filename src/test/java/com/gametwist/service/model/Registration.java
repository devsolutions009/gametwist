package com.gametwist.service.model;

public class Registration {

	private String firstName;
	private String lastName;
	private boolean isMale;
	private String countryCode;
	private String city;
	private String zip;
	private String street;
	private String phonePrefix;
	private String phoneNumber;
	private String securityQuestionTag;
	private String securityAnswer;

	
	public Registration(String firstName, String lastName, boolean isMale, String countryCode, String city, String zip,
			String street, String phonePrefix, String phoneNumber, String securityQuestionTag, String securityAnswer) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.isMale = isMale;
		this.countryCode = countryCode;
		this.city = city;
		this.zip = zip;
		this.street = street;
		this.phonePrefix = phonePrefix;
		this.phoneNumber = phoneNumber;
		this.securityQuestionTag = securityQuestionTag;
		this.securityAnswer = securityAnswer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean getIsMale() {
		return isMale;
	}

	public void setIsMale(boolean isMale) {
		this.isMale = isMale;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPhonePrefix() {
		return phonePrefix;
	}

	public void setPhonePrefix(String phonePrefix) {
		this.phonePrefix = phonePrefix;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSecurityQuestionTag() {
		return securityQuestionTag;
	}

	public void setSecurityQuestionTag(String securityQuestionTag) {
		this.securityQuestionTag = securityQuestionTag;
	}

	public String getSecurityAnswer() {
		return securityAnswer;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	@Override
	public String toString() {
		return "Registration [firstName=" + firstName + ", lastName=" + lastName + ", isMale=" + isMale
				+ ", countryCode=" + countryCode + ", city=" + city + ", zip=" + zip + ", street=" + street
				+ ", phonePrefix=" + phonePrefix + ", phoneNumber=" + phoneNumber + ", securityQuestionTag="
				+ securityQuestionTag + ", securityAnswer=" + securityAnswer + "]";
	}

	
}
