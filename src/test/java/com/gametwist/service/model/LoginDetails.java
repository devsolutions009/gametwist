package com.gametwist.service.model;

public class LoginDetails {

	private String nickname;
	private String password;
	private boolean autologin;

	public LoginDetails(String nickname, String password, boolean autologin) {
		super();
		this.nickname = nickname;
		this.password = password;
		this.autologin = autologin;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAutologin() {
		return autologin;
	}

	public void setAutologin(boolean autologin) {
		this.autologin = autologin;
	}

	@Override
	public String toString() {
		return "LoginDetails [nickname=" + nickname + ", password=" + password + ", autologin=" + autologin + "]";
	}

}
