package com.gametwist.service.core;

import com.gametwist.utils.http.RestClient;

/**
 * This class is for sharing test context between different StepDefinition files
 */
public class TestContext {

	private ScenarioContext scenarioContext;
	private DriverManager driverManger;
	private RestClient restClient;

	public TestContext() {
		scenarioContext = new ScenarioContext();
		driverManger = new DriverManager();
		restClient = new RestClient();
	}

	public ScenarioContext getScenarioContext() {
		return scenarioContext;
	}

	public DriverManager getdriverManger() {
		return driverManger;
	}

	public RestClient restClient() {
		return restClient;
	}
}