package com.gametwist.service.core;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is for usually sharing details between scenarios.
 */
public class ScenarioContext {

	private Map<String, Object> scenarioContext;

	public ScenarioContext() {
		scenarioContext = new HashMap<String, Object>();
	}

	public void setContext(String key, Object value) {
		scenarioContext.put(key.toString(), value);
	}

	public Object getContext(String key) {
		return scenarioContext.get(key.toString());
	}

	public Boolean isContains(String key) {
		return scenarioContext.containsKey(key.toString());
	}
}