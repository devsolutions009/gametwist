package com.gametwist.service.core;

import static com.gametwist.utils.Constants.CHROME;
import static com.gametwist.utils.Constants.CHROMEDRIVER_PATH;
import static com.gametwist.utils.Constants.FIREFOX;
import static com.gametwist.utils.Constants.GECKODRIVER_PATH;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * It contains pool of web drivers for desktop web applications.
 *
 */
public class DriverPool {

	/**
	 * It will get the WebDriver for specified browser.
	 * 
	 * @param browser
	 * @param nodeURL
	 * @return WebDriver
	 */
	public static WebDriver getDriver(String browser, String... nodeURL) {

		WebDriver driver = null;
		try {
			if (!(nodeURL.length == 0)) {
				System.out.println("Getting Remote web driver for " + browser + "and node URL :" + nodeURL[0]);
				driver = getRemoteDriver(browser, nodeURL[0]);
			} else {
				driver = getWebDriver(browser);
				System.out.println("Getting web driver for browser " + browser);
			}
		} catch (Exception e) {
			System.out.println("!!!!!!!! Exception occurred while getting webdriver " + e.getMessage());
		}

		return driver;
	}

	/**
	 * @param browser browser name
	 * @param nodeURL node URL where want to run execution
	 * @return RemoteWebDriver corresponding to the given browser value
	 * @throws MalformedURLException
	 */
	public static WebDriver getRemoteDriver(String browser, String nodeURL) throws MalformedURLException {
		DesiredCapabilities cap = new DesiredCapabilities();
		switch (browser.toLowerCase()) {
		case CHROME:
			cap = DesiredCapabilities.chrome();
			break;
		case FIREFOX:
			cap = DesiredCapabilities.firefox();
			break;
		default:
			cap = DesiredCapabilities.chrome();
			break;
		}
		cap.setPlatform(
				Platform.extractFromSysProperty(System.getProperty("os.name"), System.getProperty("os.version")));

		return new RemoteWebDriver(new URL(nodeURL), cap);
	}

	/**
	 * @param browser browser name
	 * @return WebDriver corresponding to the given browser value
	 * @throws MalformedURLException
	 */
	public static WebDriver getWebDriver(String browser) throws MalformedURLException {
		DesiredCapabilities cap = new DesiredCapabilities();
		WebDriver driver = null;
		switch (browser.toLowerCase()) {
		case CHROME:
			driver = getChromeDriver(cap);
			break;
		case FIREFOX:
			driver = getFirefoxGeckoDriver();
			break;
		default:
			driver = getChromeDriver(cap);
			break;
		}
		cap.setPlatform(
				Platform.extractFromSysProperty(System.getProperty("os.name"), System.getProperty("os.version")));

		return driver;
	}

	/**
	 * @return instance of firefox gecko driver
	 */
	public static WebDriver getFirefoxGeckoDriver() {
		System.setProperty("webdriver.gecko.driver", GECKODRIVER_PATH);

		return new FirefoxDriver();
	}

	/**
	 * @return instance of chrome driver
	 */
	public static WebDriver getChromeDriver(DesiredCapabilities cap) {
		System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_PATH);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");

		return new ChromeDriver(options);
	}
}
