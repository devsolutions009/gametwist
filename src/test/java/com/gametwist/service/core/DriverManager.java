package com.gametwist.service.core;

import static com.gametwist.utils.Constants.BROWSER;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.gametwist.utils.ReadProperties;

/**
 * This class is responsible for driver creation and closing/terminating
 */
public class DriverManager {
	private WebDriver driver;

	public WebDriver getDriver() {
		if (driver == null)
			driver = createDriver();

		return driver;
	}

	private WebDriver createDriver() {
		String browserName = ReadProperties.getProperty(BROWSER);
		WebDriver driver = DriverPool.getDriver(browserName);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		return driver;
	}
	
	public void captureScreenshot(WebDriver driver, String screenshotName) {
	 try {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(source, new File("target/ScreenShots/" + screenshotName + ".PNG"));
        System.out.println("ScreenShot Taken");
 	} catch (Exception e) {
        System.out.println("Exception while taking ScreenShot " + e.getMessage());
 		}
    }
	 
	public void closeDriver() {
		driver.close();
		driver.quit();
	}

}